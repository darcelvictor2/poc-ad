# Getting Started with Create React App

Plan and manage users'appointments. For this, we use the following package [https://www.npmjs.com/package/@syncfusion/ej2-react-schedule].
As a user, you have the ability to:
- plan an appointment
- delete an appointment
- modify an appointment
- check an appointment
- navigate in your calendar

## Installation

```bash
$ yarn install
```

## Runnig the application

```bash
$ yarn run start
```